# Tutorial

El comando `ssh-agent` es útil para evitar introducir por parametro la PrivateKey cada vez que te conectas a una máquina remota mediante SSH.

```
Client SSH >>> Server SSH
```

1. Requisitos previos:
* Tienes un host remoto con el demonio SSH en ejecución y accesible a través de la red.
* Conoces la dirección IP o el nombre de host y las credenciales para iniciar sesión en el host remoto.
* Has generado un par de claves SSH con una frase de paso y has transferido la clave pública a la máquina remota.

2. Pasos:
* Comprueba que puedes utilizar la clave para autenticarte en el host remoto:
```bash
$ ssh example.user1@198.51.100.1 hostname
```

* Inicia el `ssh-agent`:
```bash
$ eval $(ssh-agent)
```

* Añade la clave a `ssh-agent`:
```bash
$ ssh-add ~/.ssh/id_rsa
```

3. Verificación:
Inicia sesión en el equipo anfitrión mediante SSH:
```bash
$ ssh example.user1@198.51.100.1
```


# Guided Exercise: Configure SSH Key-based Authentication

1. Generate another set of SSH keys with passphrase-protection. Save the key as `/home/operator1/.ssh/key2`. Use redhatpass as the passphrase of the private key.
```bash
[operator1@serverb ~]$ ssh-keygen -f .ssh/key2
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase): redhatpass
Enter same passphrase again: redhatpass
Your identification has been saved in .ssh/key2.
Your public key has been saved in .ssh/key2.pub.
The key fingerprint is:
SHA256:OCtCjfPm5QrbPBgqb operator1@serverb.lab.example.com
The key's randomart image is:
+---[RSA 3072]----+
|O=X*             |
|OB=.             |
|E*o.             |
|Booo   .         |
|..= . o S        |
| +.o   o         |
|+.oo+ o          |
|+o.O.+           |
|+ . =o.          |
+----[SHA256]-----+
```

2. Send the public key of the passphrase-protected key pair to the `operator1` user on the servera machine. The command does not prompt you for a password, because it uses the public key of the passphrase-less private key that you exported to the servera machine in the preceding step.
```bash
[operator1@serverb ~]$ ssh-copy-id -i .ssh/key2.pub operator1@servera
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: ".ssh/key2.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys

Number of key(s) added: 1

Now try logging in to the machine, with:   "ssh 'operator1@servera'"
and check to make sure that only the key(s) you wanted were added.
```

3. Execute the `hostname` command on the `servera` machine remotely by using the ssh command. Use the `/home/operator1/.ssh/key2` key as the identity file. Specify redhatpass as the passphrase, which you set for the private key in the preceding step.

```bash
[operator1@serverb ~]$ ssh -i .ssh/key2 operator1@servera hostname
Enter passphrase for key '.ssh/key2': redhatpass
servera.lab.example.com
```

4. Run the `ssh-agent` program in your Bash shell, and add the passphrase-protected private key (`/home/operator1/.ssh/key2`) of the SSH key pair to the shell session.
```bash
[operator1@serverb ~]$ eval $(ssh-agent)
Agent pid 1729
[operator1@serverb ~]$ ssh-add .ssh/key2
Enter passphrase for .ssh/key2: redhatpass
Identity added: .ssh/key2 (operator1@serverb.lab.example.com)
```

5. Execute the `hostname` command on the `servera` machine remotely without accessing a remote interactive shell. Use the `/home/operator1/.ssh/key2` key as the identity file.

The command does not prompt you to enter the passphrase interactively.
```bash
[operator1@serverb ~]$ ssh -i .ssh/key2 operator1@servera hostname
servera.lab.example.com
```

6. Open another terminal on the workstation machine and log in to the `serverb` machine as the `student` user.
```bash
[student@workstation ~]$ ssh student@serverb
...output omitted...
[student@serverb ~]$
```

Use the `su` command to switch to the `operator1` user. Use redhat as the password for the `operator1` user.
```bash
[student@serverb ~]$ su - operator1
Password: redhat
[operator1@serverb ~]$
```

7. Log in to the servera machine as the `operator1` user.

The command prompts you to enter the passphrase interactively, because you do not invoke the SSH connection from the same shell where you started the `ssh-agent` program.
```bash
[operator1@serverb ~]$ ssh -i .ssh/key2 operator1@servera
Enter passphrase for key '.ssh/key2': redhatpass
...output omitted...
[operator1@servera ~]$
```

# Troubleshooting

## En el ssh-client
Lists fingerprints of all identities currently represented by the agent.
```bash
MBP:~ jean$ ssh-add -l
3072 SHA256:Wi7MdMw3gTA+Ib9SPC9lNAdm0GTyQ/TjbbMDIXSwuo8 jean@SERVER.local (RSA)
```

Comprobamos que nuestro Private Key este añadido en la Lists Fingerprints
```bash
MBP:SSH jean$ ssh-keygen -lf id_rsa172_16_21_142
3072 SHA256:Wi7MdMw3gTA+Ib9SPC9lNAdm0GTyQ/TjbbMDIXSwuo8 jean@SERVER.local (RSA)
```bash


