# ProxyJump

```bash
ssh -o ProxyCommand="ssh -l devops -W %h:%p 172.18.1.14" root@10.10.1.200
```

```bash
Host linux_salto
  Hostname 172.18.1.14
  User devops

Host server-s1
  Hostname 10.10.1.200
  User root
  ProxyJump linux_salto
```

```bash
ssh -J devops@172.18.1.14 root@10.10.1.200
```
